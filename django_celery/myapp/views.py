from django.shortcuts import render
from django.http import HttpResponse
import requests
from myapp.tasks import adding_task, add, mul


def index(request):
    task = adding_task.delay(2,5)
    print(f"id={task.id}, state={task.state}, status={task.status}")
    # task.get()
    print("e")
    return HttpResponse("Go go power rangers")


def err(request):
    # task = mul.apply_async(2,5,)
    # task = mul.apply(2,5)
    task = mul.delay(1,1)
    # task = mul(2,5)
    # mul(2,3)
    print(f"id={task.id}, state={task.state}, status={task.status}")
    # task.get()
    return HttpResponse("Go go power rangers")